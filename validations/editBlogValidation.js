const JOI = require('joi');

const blogSchema = JOI.object({
  _id: JOI.string()
    .required(),
  userId: JOI.string()
    .required(),
  title: JOI.string()
    .required(),
  tags: JOI.string()
    .required(),
  body: JOI.string()
    .required(),
  createdAt: JOI.date()
    .required(),
  updatedAt: JOI.date()
    .required()
});
const editBlogValidation = (data) => blogSchema.validate(data);
module.exports.editBlogValidation = editBlogValidation;
