const JOI = require('joi');

const blogSchema = JOI.object({
  userId: JOI.string()
    .required(),
  title: JOI.string()
    .required(),
  tags: JOI.string()
    .required(),
  body: JOI.string()
    .required()
});
const blogValidation = (data) => blogSchema.validate(data);
module.exports.blogValidation = blogValidation;
