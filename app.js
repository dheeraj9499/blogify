const express = require('express');
const chalk = require('chalk');
const cookieParser = require('cookie-parser');
const path = require('path');
require('dotenv').config();
// const debug = require('debug')('app');
const mongoose = require('mongoose');
const morgan = require('morgan');

const app = express();
const appHomeRoutes = require('./routes/appHomeRoutes');
const authRoutes = require('./routes/authRoutes');
const editorRoutes = require('./routes/editorRoutes');
const userRoutes = require('./routes/userRoutes');
const blogRoutes = require('./routes/blogRoutes');
// const secret = 'abcdefgh';
// to output the request url, status, res time | ms etc
app.use(morgan('tiny'));
app.use(express.static('public'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
const CHECKUSER = require('./routes/checkUser');

// body-parser
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(cookieParser());
// listen for requests
const port = process.env.PORT || 3000;
const dbURI = process.env.DB_URI;

mongoose.connect(dbURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => console.log(chalk.whiteBright.underline('Connected to db!!')))
  .then(() => {
    app.listen(port, () => console.log(`Server listening on port ${chalk.green.italic(port)}`));
  })
  .catch((err) => console.log(err));

app.get('*', CHECKUSER);
app.use('/', appHomeRoutes);
app.use('/auth', authRoutes);
app.use('/editor', editorRoutes);
app.use('/u', userRoutes);
app.use('/blog', blogRoutes);
