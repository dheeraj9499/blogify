const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { Schema } = mongoose;

const userSchema = new Schema({
  username: {
    type: String,
    trim: true,
    min: 6,
    max: 128,
    required: true
  },
  email: {
    type: String,
    trim: true,
    min: 6,
    max: 255,
    required: true
  },
  password: {
    type: String,
    minLength: 8,
    maxLength: 255,
    required: true
  }
}, {
  timestamps: true,
  versionKey: false
});

userSchema.methods.generateHash = async (password) => {
  const hashedKey = await bcrypt.hash(password, 10);
  return hashedKey;
};
userSchema.methods.compareHash = async (password, hash) => {
  const isValid = await bcrypt.compare(password, hash);
  return isValid;
};

module.exports = mongoose.model('user', userSchema);
