const mongoose = require('mongoose');

const { Schema } = mongoose;

const blogSchema = new Schema({
  userId: {
    type: String,
    required: true,
    ref: 'user'
  },
  title: {
    type: String,
    trim: true,
    required: true
  },
  tags: {
    type: String,
    trim: true,
    required: true
  },
  body: {
    type: String,
    trim: true,
    required: true
  }
}, {
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model('blog', blogSchema);
