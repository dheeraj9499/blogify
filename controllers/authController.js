const jwt = require('jsonwebtoken');
const { loginValidation } = require('../validations/loginValidation');
const { signupValidation } = require('../validations/signupValidation');
const User = require('../models/userModel');

const loginPost = async (req, res) => {
  res.locals.user = null;
  const { error } = loginValidation(req.body);
  if (error) {
    res.statusCode = 404;
    const errorMessage = error.details[0].message;
    return res.render('auth/auth', { errorMessage });
  }
  const { email, password } = req.body;
  User.findOne({ email }, async (err, user) => {
    if (err) {
      return res.send('Error occured');
    }
    if (!user) {
      const errorMessage = 'Email Incorrect';
      return res.render('auth/auth', { errorMessage });
    }
    const isValid = await user.compareHash(password, user.password);
    if (!isValid) {
      const errorMessage = 'Password Incorrect';
      return res.render('auth/auth', { errorMessage });
    }
    const token = jwt.sign({ _id: user._id, username: user.username }, process.env.SECRET, { expiresIn: '3 days' });
    res.locals.user = {
      _id: user._id,
      username: user.username
    };
    // cookie age 3 days, can't access through the dom access only transferred via http protocol
    res.cookie('accessToken', token, { maxAge: 1000 * 60 * 60 * 72, httpOnly: true });
    return res.redirect('/');
  });
};

const loginSignupGet = (req, res) => {
  if (res.locals.user !== null) {
    res.redirect('/');
  }
  res.render('auth/auth');
};

const redirectToAuth = (req, res) => {
  res.redirect('/auth');
};

const logout = (req, res) => {
  res.cookie('accessToken', '', { maxAge: 1 });
  res.redirect('/');
};

const signupPost = async (req, res) => {
  res.locals.user = null;
  const { error } = signupValidation(req.body);
  if (error) {
    const errorMessage = error.details[0].message;
    return res.render('auth/auth', { errorMessage });
  }
  const emailExists = await User.findOne({
    email: req.body.email,
  });
  if (emailExists) {
    const errorMessage = 'Email Already Exists!';
    return res.render('auth/auth', { errorMessage });
  }
  const { username, email, password } = req.body;
  const user = new User();
  user.username = username;
  user.email = email;
  try {
    user.password = await user.generateHash(password);
  } catch (err) {
    res.send('Something went wrong', err);
  }
  res.locals.user = {
    _id: user._id,
    username: user.username
  };
  user
    .save()
    .then(() => {
      const token = jwt.sign({ _id: user._id, username: user.username }, process.env.SECRET, { expiresIn: '3 days' });
      res.cookie('accessToken', token, { maxAge: 1000 * 60 * 60 * 72, httpOnly: true });
      res.redirect('/');
    })
    .catch(() => res.send('Something went wrong!'));
};

module.exports = {
  signupPost,
  loginPost,
  loginSignupGet,
  logout,
  redirectToAuth
};
