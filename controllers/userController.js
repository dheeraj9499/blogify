const Blog = require('../models/blogModel');

const myBlogs = async (req, res) => {
  const id = res.locals.user._id;
  try {
    const data = await Blog.find({ userId: id }, {
      _id: 1, tags: 1, title: 1, userId: 1, createdAt: 1
    }).sort('-createdAt');
    res.render('user/myblogs', { data });
  } catch (err) {
    res.send('Something went wrong, try again after some time');
  }
};

module.exports.myBlogs = myBlogs;
