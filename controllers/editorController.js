const { decode } = require('jsonwebtoken');
const { blogValidation } = require('../validations/blogValidation');
const Blog = require('../models/blogModel');

const editorMain = (req, res) => {
  const data = null;
  res.render('editor/editor', { data });
};

const editorSaveBlog = async (req, res) => {
  const data = req.body;
  const id = decode(req.cookies.accessToken)._id;
  data.userId = id;
  const { error } = blogValidation(data);
  if (error) {
    const errorMessage = error.details[0].message;
    return res.render('editor/editor', { errorMessage });
  }
  const blog = new Blog(data);
  try {
    await blog.save();
    res.redirect('/u/myblogs');
  } catch (err) {
    res.send('Something went wrong!!');
    res.end();
  }
};

module.exports = {
  editorMain,
  editorSaveBlog
};
