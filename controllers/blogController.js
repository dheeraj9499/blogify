const { verify } = require('jsonwebtoken');
const Blog = require('../models/blogModel');
const { editBlogValidation } = require('../validations/editBlogValidation');

const homeBlogs = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Blog.findById(id).populate('userId', 'username');
    res.render('blog/blogUI', { data });
  } catch (err) {
    res.send('Something went wrong!!');
  }
};

const editBlog = async (req, res) => {
  const token = req.cookies.accessToken;
  const { _id } = verify(token, process.env.SECRET);
  try {
    const { id } = req.params;
    const data = await Blog.findById(id);
    if (data.userId !== _id) {
      return res.send('Unauthorized');
    }
    res.render('blog/blogEdit', { data });
  } catch (err) {
    res.send('Something went wrong!!');
  }
};

const saveBlog = async (req, res) => {
  const data = req.body;
  const { error } = editBlogValidation(data);
  if (error) {
    const errorMessage = error.details[0].message;
    return res.render('blog/blogEdit', { data, errorMessage });
  }
  try {
    const { _id } = data;
    const blog = await Blog.findById(_id);
    Object.assign(blog, data);
    await blog.save();
    res.redirect('/u/myblogs');
  } catch (err) {
    res.send('Something went wrong!!');
    res.end();
  }
};

const deleteBlog = async (req, res) => {
  const token = req.cookies.accessToken;
  const { _id } = verify(token, process.env.SECRET);
  try {
    const { id } = req.params;
    const blog = await Blog.findById(id);
    if (blog.userId !== _id) {
      return res.send('Unauthorized');
    }
    await blog.remove();
    res.redirect('/u/myblogs');
  } catch (err) {
    res.send('Something went wrong!!');
  }
};
module.exports = {
  homeBlogs,
  editBlog,
  saveBlog,
  deleteBlog
};
