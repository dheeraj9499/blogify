const Blog = require('../models/blogModel');

const loadHomeBlogs = async (req, res) => {
  try {
    const data = await Blog.find().populate('userId', 'username');
    res.render('main/home', { data });
  } catch (err) {
    res.status = 404;
    res.send('Something went wrong, try again after some time!');
  }
};

module.exports.loadHomeBlogs = loadHomeBlogs;
