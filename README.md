# BlogPost Web App

It is a Blog web app in which user can write their blogs (ofc with crud operation), consisting of an editor which help visualize user that how the blog will look like.

<img src="https://media.giphy.com/media/SsULkEGLdK1lij4dOi/giphy.gif" width="500" height="400" />

## Features

- WYSIWYG Editor
- Authentication/Authorization using JWT
- Blog CRUD

## Tech

- [EJS] - Templating language that lets you generate HTML markup with plain JavaScript
- [SCSS] - Stylesheets
- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework
- [JWT] - JWT.IO allows you to decode, verify and generate JWT (Authentication/Authorization).
- [Mongodb] - MongoDB is a general purpose, document-based, distributed database built for modern application developers and for the cloud (use mongodb atlas, if possible)

## Installation

Open terminal/bash

```sh
mkdir blogpost
cd blogpost
git clone https://gitlab.com/dheeraj9499/blogify.git
```
or you can clone the project from https://gitlab.com/dheeraj9499/blogify.git

1. make a **.env** file in the project root directory
2. paste follwing lines to the **.env** file
3. replace the variable accordingly.

Install the dependencies and devDependencies and start the server.

```sh
PORT = <PORT-NO>
DB_URI = <MONGODB_CONNECTION_STRING>
DATABASE_NAME = <DATABASE_NAME>
SECRET = <TOKEN_SECRET_STRING>
```
then finally 😅
```sh
npm install
npm start
```
👍👍👍

   [node.js]: <http://nodejs.org>
   [express]: <http://expressjs.com>
   [EJS]: <https://ejs.co/>
   [SCSS]: <https://sass-lang.com/>
   [MongoDB]: <https://www.mongodb.com//>
   [JWT]: <https://jwt.io/>

