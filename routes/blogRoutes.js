const router = require('express').Router();
const AUTHUSER = require('./verifyUser');
const blogController = require('../controllers/blogController');

router.get('/:id', blogController.homeBlogs);

router.get('/edit/:id', AUTHUSER, blogController.editBlog);

router.post('/edit/save', AUTHUSER, blogController.saveBlog);

router.get('/delete/:id', AUTHUSER, blogController.deleteBlog);

module.exports = router;
