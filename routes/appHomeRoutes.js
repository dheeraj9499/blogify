const router = require('express').Router();
const { loadHomeBlogs } = require('../controllers/appHomeController');

router.get('/', loadHomeBlogs);

module.exports = router;
