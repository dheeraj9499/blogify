const router = require('express').Router();
const AUTHTOKEN = require('./verifyUser');
const { myBlogs } = require('../controllers/userController');

router.get('/myblogs', AUTHTOKEN, myBlogs);

module.exports = router;
