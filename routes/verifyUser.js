const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  const token = req.cookies.accessToken;
  res.locals.user = null;
  if (!token) {
    return res.render('auth/auth');
  }
  try {
    const payload = jwt.verify(token, process.env.SECRET);
    res.locals.user = payload;
    next();
  } catch (err) {
    return res.render('auth/auth');
  }
};
