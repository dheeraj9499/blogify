const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  const token = req.cookies.accessToken;
  try {
    const payload = jwt.verify(token, process.env.SECRET);
    res.locals.user = payload;
    next();
  } catch (err) {
    res.locals.user = null;
    next();
  }
};
