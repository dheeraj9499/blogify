const router = require('express').Router();
const authController = require('../controllers/authController');

router.get('/', authController.loginSignupGet);

router.route('/login').post(authController.loginPost);

router.get('/logout', authController.logout);

router.route('/signup').post(authController.signupPost);

router.get('/*', authController.redirectToAuth);

module.exports = router;
