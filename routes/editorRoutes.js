const router = require('express').Router();
const AUTHTOKEN = require('./verifyUser');
const { editorMain, editorSaveBlog } = require('../controllers/editorController');

router.get('/*', editorMain);

router.post('/save', AUTHTOKEN, editorSaveBlog);

module.exports = router;
